# Mangr0ve

Mangr0ve is a terminal application that makes rolling back to any previous btrfs snapshot of your `/` subvolume trivial


# Images

![Screenshot of UI when first invoking Mangr0ve. You're asked which snapshot you wish to boot from, and presented with a list of snapshots and their metadata](images/screenshot_1.png "Initial UI of Mangr0ve"){width=30%} ![Screenshot of UI after entering the number of target snapshot. You're told that the snapshot is set up for next boot, and that old rw snapshots are removed. Mangr0ve then exits](images/screenshot_2.png "UI of Mangr0ve after finishing"){width=30%}

# Requirements

* The filesystem must have a flat btrfs subvolume layout
* Snapshots must be compatible with `snapper`'s format
* Mangr0ve is developed for busybox ash shell, and not tested for other shells


# How to use

1. Invoke `mangr0ve` as root
2. Decide which snapshot you wish to set as your `/` subvol from among the listed snapshots
3. Enter the name (number) of your target snapshot
4. Reboot

A read-write snapshot of the chosen read-only snapshot is now set as default `/` subvolume for the filesystem, and your system has been rolled back.


# License
This project is licensed under the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.


# Shoutouts to projects

[PostmarketOS](https://postmarketos.org)

[apk-snap](https://gitlab.com/papiris/apk-snap)

[snapper](https://github.com/openSUSE/snapper)
