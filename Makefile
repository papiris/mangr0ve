# mangr0ve
# https://gitlab.com/papiris/mangr0ve
# Copyright (C) 2024 Jacob D. Ludvigsen

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

PKGNAME = mangr0ve
PREFIX ?= /usr

SHARE_DIR = $(DESTDIR)$(PREFIX)/share

.PHONY: install test docs

install:
	@install -Dm755 scripts/mangr0ve.sh $(DESTDIR)$(PREFIX)/bin/mangr0ve
	@install -Dm644 COPYING -t $(SHARE_DIR)/licenses/$(PKGNAME)/
	@install -Dm644 README.md -t $(SHARE_DIR)/doc/$(PKGNAME)/
